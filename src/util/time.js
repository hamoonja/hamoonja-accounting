module.exports = {
  getTimeInSeconds(date) {
    return (date || +new Date) / 1000 | 0
  }
}