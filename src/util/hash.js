const hash = require('crypto').createHash
const {alg, digest} = {alg: 'sha1', digest: 'base64'}


module.exports = {
    getUniqueFor(data) {
        return hash(alg).update(data).digest(digest)
    }
}