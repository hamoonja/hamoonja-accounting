const proc = require('../services').payment

async function createPaymentUrl(req, res) {
  try {
    const url = await proc.getPaymentUrl(req.query)
    return { url }
  } catch (e) {
    console.log('error in /payments:', e)
    res.status(400)
  }
}

async function processPaymentResult(req, res) {
  try {
    const { 
      query: {
        Status: status,
        Authority: token,
        amount,
        capacity,
        expDays,
        mobileNumber,
        meetingTitle,
      }
    } = req
    if (status !== 'OK') {
      return res.redirect(`https://hamoonja.com/payment/result?status=${status}`)
    }
    const paymentRef = await proc.verifyPayment({ amount, token })
    const url = await proc.processSuccessfulPayment({ amount, capacity, expDays, mobileNumber, meetingTitle})
    
    return res.redirect(`https://hamoonja.com/payment/result?status=${
        status
      }&paymentRef=${
        paymentRef
      }&url=${
        encodeURIComponent(url)
      }`)
  } catch (error) {
    console.log(err)
    return res.redirect(`https://hamoonja.com/payment/result?error=true`)
  }
}

module.exports = server => {
  server.get('/payments', createPaymentUrl)
  server.get('/payments/result', processPaymentResult)
}
