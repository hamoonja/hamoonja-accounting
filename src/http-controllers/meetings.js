const proc = require('../services').meeting


async function createTest(req, res) {
    const is_test = req.body.is_test
    if (!is_test) {
        return res.status(400).send('no')
    }
    try {
        const testMeeting = await proc.addTest()
        return {url: `https://meet.hamoonja.com/${testMeeting.slug}`};
    } catch (e) {
        console.log(`Cannot get meeting from controller: ${e}`)
        res.status(400).send('no')
    }
}

async function canJoin(req, res) {
    const userIP = req.headers['x-forwarded-for'] || req.connection.remoteAddress
    const slug = req.query.slug
    const userRandID = req.query.entropy
    const deviceWidth = req.query.deviceWidth
    const deviceHeight = req.query.deviceHeight

    const userAgent = req.query.userAgent || req.headers['user-agent']

    try {
        const meeting = await proc.canJoin(
            {
                slug,
                userIP,
                userRandID,
                userAgent,
                deviceWidth,
                deviceHeight,
            }
        )
        if (meeting) {
            return res.send('yes')
        }
        return res.status(400).send('no')
    } catch (e) {
        console.log(`Cannot query meeting status : ${JSON.stringify(e)}`)
        res.status(400).send('no')
    }
}

async function getMeeting(req, res) {
    const slug = req.params['slug']
    const meeting = await proc.findOne({slug})
    if (!meeting) {
        res.status(404).send('Not found')
    }
    res.send({title: meeting.title})

}

module.exports = server => {
    server.get('/meetings', canJoin)
    server.post('/meetings', createTest)
    server.get('/meetings/:slug', getMeeting)
}

