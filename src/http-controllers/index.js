module.exports = server => {
    require('./meetings')(server)
    require('./payments')(server)
}