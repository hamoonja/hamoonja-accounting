const every = require('every.js')

every('60 seconds', require('../services/meeting').purgeStaleUsers)
