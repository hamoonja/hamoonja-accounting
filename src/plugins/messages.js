const { convertNumbersToPersian } = require('./../util/strings')

function paymentDescription(capacity, expDays){
  const persianCapacity = convertNumbersToPersian(capacity)
  const persianExp = convertNumbersToPersian(expDays)
  return `اجاره اتاق جلسه‌ی مجازی ${persianCapacity} نفره به مدت ${persianExp} روز`;
}

module.exports = {
  paymentDescription,
}