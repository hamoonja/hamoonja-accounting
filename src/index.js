require('dotenv').config()
const fastify = require('fastify')({ logger: true })
require('./http-controllers')(fastify)
require('./jobs')

// Run the server!
const start = async () => {
    try {
        fastify.register(require('fastify-cors'), {
            origin: ['https://hamoonja.com', /\.hamoonja\.com$/]
        })
        await fastify.listen(process.env.PORT || 4500, '0.0.0.0')
        fastify.log.info(`server listening on ${fastify.server.address().port}`)
    } catch (err) {
        fastify.log.error(err)
        process.exit(1)
    }
}

(async () => {
    await start()
})()