const Zarinpal = require('zarinpal-checkout')
const moment = require('moment')
const {paymentDescription} = require('./../../plugins').messages
const meeting = require('../meeting')
const {calculatePrice} = require('../../shared/price')

const isSandbox = process.env.NODE_ENV !== 'production'
if (isSandbox) {
  console.log(`zarinpal is in Sandbox = ${isSandbox}`)
}

const baseUrl = isSandbox ? 'http://localhost:4500' : 'https://api.hamoonja.com';

const zarinpal = Zarinpal.create(process.env.ZARINPAL_MERCHANT_CODE, isSandbox)

async function getPaymentUrl({capacity, expDays, amount, mobileNumber, meetingTitle}) {
  const paymentInfo = await zarinpal.PaymentRequest({
    Amount: amount,
    CallbackURL: `${baseUrl}/payments/result?capacity=${capacity}&expDays=${expDays}&amount=${amount}&mobileNumber=${mobileNumber}&meetingTitle=${meetingTitle}`,
    Description: paymentDescription(capacity, expDays),
  })
  if (paymentInfo.status === 100) {
    return paymentInfo.url
  }
  throw new Error('something went wrong in getting payment url')
}

async function verifyPayment({amount, token}) {
  const {status, RefID: ref} = await zarinpal.PaymentVerification({
    Amount: amount,
    Authority: token,
  })
  if (status === 100 || status === 101) return ref
  throw new Error('Payment not verified')
}

async function processSuccessfulPayment({
                                          capacity,
                                          expDays,
                                          amount,
                                          mobileNumber,
                                          meetingTitle,
                                        }) {

  // TODO: @javad Should return an error
  if (Number.parseInt(amount) != calculatePrice(capacity, expDays)) {
    return ''
  }
  const expires_in = new Date(moment().add(expDays, 'd'))
  // Allow up to 3 more users to join a meeting to avoid any inconvinience
  const extendedCapacity = capacity + 3
  // TODO: @javad handle errors (if meeting is null, cannot create meeting etc)
  try {
    const newMeeting = await meeting.add({
      extendedCapacity,
      expires_in,
      amount_paid: amount,
      owner_mobile: mobileNumber,
      title: meetingTitle,
    })

    return `https://meet.hamoonja.com/${newMeeting.slug}`
  } catch (e) {
    return `https://hamoonja.com`
  }
}


module.exports = {
  getPaymentUrl,
  verifyPayment,
  processSuccessfulPayment,
}