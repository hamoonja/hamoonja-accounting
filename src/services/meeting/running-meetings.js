const r = require('../redis-adapter')
const {getTimeInSeconds} = require('../../util/time')

const keyPrefix = 'meeting:'

async function find({slug}) {
  const strObj = await r.get(getPrefixedKey(slug))
  return JSON.parse(strObj)
}

async function set({slug, data}) {
  return r.set(getPrefixedKey(slug), JSON.stringify(data))
}

async function getAll() {
  return r.keys(`${keyPrefix}*`)
}

function getPrefixedKey(originalKey) {
  return `${keyPrefix}${originalKey}`
}

async function purgeStaleUsers() {
  const list = await getAll()
  list.forEach(l => purge({slug: l.replace(keyPrefix, '')}))

}

//// Auxiliary

async function purge({slug}) {
  // in seconds
  const timeThresholdToRemove = 60
  const meeting = await find({slug})
  if (!meeting) {
    console.log('Error getting running meetings')
    return
  }

  try {
    const now = getTimeInSeconds()
    const filteredParticipants = {}
    Object.keys(meeting.participants).forEach(k => {
      if (now - meeting.participants[k] >= timeThresholdToRemove ) {
        filteredParticipants[k] = meeting.participants[k]
      }
    })
    meeting.participants = filteredParticipants
    await set({slug, data: meeting})
  } catch (e) {
    console.log('Error in purging stale users', e)
  }


}


module.exports = {find, set, getAll, purgeStaleUsers}