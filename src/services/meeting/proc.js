const moment = require('moment')

const store = require('./store')
const runningMeetings = require('./running-meetings')

const {getUniqueFor} = require("../../util/hash")
const {getTimeInSeconds} = require("../../util/time")

async function add(meeting) {
    return store.create(meeting)
}

async function findOne({slug}) {
    return store.findOne({slug})
}

async function getRunning() {
    return runningMeetings.getAll()
}

async function findOneInRunning({slug}) {
    return runningMeetings.find({slug})
}

async function purgeStaleUsers() {
    return runningMeetings.purgeStaleUsers()
}

// Auxiliary functions

async function canJoin({slug, userIP, userRandID, userAgent, deviceWidth, deviceHeight}) {
    const meetingData = await runningMeetings.find({slug})
    const userDataHash = hashUserData({userIP, userRandID, userAgent})

    if (meetingData) {
        return alreadyHaveMeeting({meetingData, userData: userDataHash})
    }

    // Find the meeting in db
    const meeting = await findOne({slug})

    // No meeting found with that slug so return error
    if (!meeting) {
        return Promise.reject({
            code: 400,
            data: 'No meeting found with id: ' + slug
        })
    }

    if (meeting.is_expired || isMeetingDateExpired({meetingData: meeting})) {
        return Promise.reject({
            code: 400,
            data: 'Meeting is expired'
        })
    }

    return startMeeting({slug, userData: userDataHash, meeting})
}

async function addTest() {
    const m = {
        is_test: true,
        title: 'اتاق تست',
        max_length: 5,
        capacity: 3,
        is_expired: false,
        expires_in: new Date(moment().add(30, 'm'))
    }
    try {
        return await store.create(m)
    } catch (e) {
        console.log('Room controller is unable to create room')
        return e
    }
}

async function startMeeting({slug, userData, meeting}) {

    const now = getTimeInSeconds()

    const cm = {
        slug,
        // store last pings date for user
        participants: {[userData]: now}
    }

    cm.started_at = cm.last_ping = now
    cm.max_length = meeting.max_length
    cm.is_test = meeting.is_test
    cm.capacity = meeting.capacity
    cm.expires_in = meeting.expires_in

    try {
        await runningMeetings.set({slug, data: cm})
        console.log('Unable to set value of current meeting in running meetings')
        return Promise.resolve(meeting)
    } catch (e) {
        return e
    }

}

async function alreadyHaveMeeting({meetingData, userData}) {
    if (isMeetingExceededAllowedLength({meetingData}) || isMeetingDateExpired({meetingData})) {
        await store.updateOne({slug: meetingData.slug}, {is_expired: true})
        return Promise.reject({
            status: 400,
            data: 'Meeting is expired'
        })
    }

    if (meetingData.participants[userData]) {
        meetingData.last_ping = meetingData[userData] = getTimeInSeconds()
        return Promise.resolve({
            status: 200,
            data: meetingData
        })
    }

    const currentNumberOfParticipants = (Object.keys(meetingData.participants) || []).length

    if (currentNumberOfParticipants >= meetingData.capacity) {
        return Promise.reject({
            status: 400,
            data: 'Meeting is already full'
        })
    }

    meetingData.last_ping = meetingData.participants[userData] = getTimeInSeconds()
    await runningMeetings.set({slug: meetingData.slug, data: meetingData})

    return Promise.resolve({
        status: 200,
        data: meetingData
    })
}

function hashUserData({userIP, userRandID, userAgent, deviceWidth, deviceHeight}) {
    // Concatenate all client data together; ignore if anything is undefined or falsy by filter
    const joined = [userIP, userRandID, userAgent, deviceWidth, deviceHeight].filter(x => !!x).join('-')
    const key = getUniqueFor(joined)
    return key.substr(0, 10)

}

function isMeetingExceededAllowedLength({meetingData}) {
    const now = getTimeInSeconds()
    const startedTime = meetingData.started_at
    const allowedMeetingLength = meetingData.max_length * 60
    return (now - startedTime) > allowedMeetingLength;
}


function isMeetingDateExpired({meetingData}) {
    const now = getTimeInSeconds()
    const expires_in = getTimeInSeconds(new Date(meetingData.expires_in))
    return now > expires_in
}

module.exports = {
    add,
    findOne,
    addTest,
    canJoin,
    getRunning,
    findOneInRunning,
    purgeStaleUsers
}