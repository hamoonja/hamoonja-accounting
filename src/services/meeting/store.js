const rand = require('../../util/rand')
const Model = require('./model')

async function create(m) {
    const id = rand.mongoID().toLowerCase()
    return await Model.create({
        _id: id,
        slug: id,
        ...m
    })
}

async function findOne({slug}) {
    return Model.findOne({slug})
}

async function updateOne({slug}, newData) {
    return Model.updateOne({slug}, newData)
}


module.exports = {
    create,
    findOne,
    updateOne
}