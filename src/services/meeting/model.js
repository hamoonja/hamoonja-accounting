const mongoose = require('../mongo-adapter')
const {Schema} = mongoose

const modelName = 'meeting'

const participantSchema = {
    ip: String,
    user_agent: String,
    user_random_id: String
}

const schema = {
    slug: String,
    title: String,
    is_test: Boolean,
    capacity: Number,
    max_length: Number,
    is_expired: Boolean,
    expires_in: Date,
    owner_mobile: String,
    amount_paid: Number,
    // participants: [participantSchema],
}

const Model = mongoose.model(modelName, new Schema(schema,
    {timestamps: {createdAt: 'created_at', updatedAt: 'updated_at'}}))


module.exports = Model