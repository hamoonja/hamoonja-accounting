const ioredis = require('ioredis')
const Redis = new ioredis(process.env.REDIS_URL)
module.exports = Redis
